const path = require('path');

const minimist = require('minimist');

const pkgConfig = require('./package.json');
const logger = require('./lib/logger');
const translator = require('./lib/translator');

// Set process title
process.title = 'pouch';

// Set basedir
global.__basedir = __dirname;

// Useful functions
const help = () => {
  console.log(`${pkgConfig.name} version ${pkgConfig.version}

Usage:

  ${pkgConfig.name} [-h] [-v] [-c <file>]

  -h --help          : Display this page.
  -v --version       : print ${pkgConfig.name} version.
  -c --config <file> : Use the goven config file.`);
  process.exit(0);
};

const version = () => {
  console.log(`${pkgConfig.name} version ${pkgConfig.version}`);
  process.exit(0);
};

// Parse args
const args = minimist(process.argv.slice(2), {
  alias: {
    h: 'help',
    v: 'version',
    c: 'config',
  },
  boolean: ['h', 'v'],
  string: ['c'],
  default: {
    h: false,
    v: false,
  },
});

if (args.h) {
  help();
}
if (args.v) {
  version();
}

// Load config
if (args.c) {
  process.env.NODE_CONFIG_ENV = path.basename(args.c, path.extname(args.c));
  process.env.NODE_CONFIG_DIR = path.dirname(args.c);
}
const config = require('config');

// initialize logger
logger.initialize(config);

// main loop
config.get('translations').forEach(translator.run);
