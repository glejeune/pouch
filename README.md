## Pouch

Pouch is a simple service allowing you to stream data from one source to an other.

## Install

Pouch is written in JavaScript. You need to install node >=12.16.3

Just clone this repo and run `npm` or `yarn` to install dependencies:

```
git clone https://gitlab.com/glejeune/pouch
cd pouch
npm install # or yarn install
```

## Configuration

TBA

Example:

```
{
  "translations": [
    {
      "name": "pg_to_consul",
      "intensity": 1,
      "period": 5,
      "restart": "permanent",
      "processor": {
        "driver": "JSONToJSON",
        "args": [
          {
            "ensembl": "gene",
            "pathway": {
              "id": "id",
              "name": "name"
            }
          }
        ]
      },
      "input": {
        "driver": "Postgres",
        "args": [
          ["table1", "table2"],
          {
            "retry": 5,
            "retryWait": 1000,
            "user": "simple-web",
            "password": "simple-web",
            "database": "simple-web",
            "port": 5432,
            "host": "localhost"
          }
        ]
      },
      "output": {
        "driver": "Consul",
        "args": [
          {
            "secure": false,
            "host": "localhost",
            "port": 8500
          },
          "base/key/[name:full]"
        ]
      }
    }
  ],
  "log": {
    "file": "pouch.log",
    "level": "debug",
    "stdout": true
  }
}
```

## Available Drivers

### Input Driver

#### Filesystem

This driver will watch events (add, change, unlink) from the filesystem.

This driver takes two arguments :

* A string, or an array of string, containing the file, dir or glob to watch.
* An object of [chokidar options](https://github.com/paulmillr/chokidar). If you want to use the `ignored` chokidar option you must use a regexp string as value for this option.

Example :

```
{
  "translations": [
    {
      "input": {
        "driver": "Filesystem",
        "args": [
          "./*.json",
          {
            "followSymlinks": true,
            "ignored": "(^|[\\/\\\\]).."
          }
        ]
      }
    }
  ]
}
```

#### Postgres

This driver will listen events (CREATE, UPDATE, DELETE) from a PostgreSQL database.

This driver takes two arguments :

* An array of string containing the names of the tables to listen events.
* An object for the postgres connection with the following keys:
  * `retry` (Integer): The number of connection retry.
  * `retryWait` (Integer): The time (in ms) between each connection retry.
  * `user` (String) The database username.
  * `password` (String) The database password.
  * `database` (String) The database name.
  * `port` (Integer) The database port.
  * `host` (String) The database hostname.

Example :

```
{
  "translations": [
    {
      "input": {
        "driver": "Postgres",
        "args": [
          ["table1", "table2"],
          {
            "retry": 5,
            "retryWait": 1000,
            "user": "my_user",
            "password": "s3cr3t",
            "database": "my_database",
            "port": 5432,
            "host": "localhost"
          }
        ]
      }
    }
  ]
}
```

### Output Driver

#### Filesystem

This driver allow to store tranlated datas in files.

This driver takes two arguments :

* A String containing the base path used to create output files.
* A String containing the template of the output file name. This template can use the following patterns :
  * `[name:full]`: The full name of the event (see Event section).
  * `[name:basename]`: The basename of the event name (see Event section).
  * `[name:extname]`: The extension of the event name (see Event section).
  * `[old:<key>]`: The value of the `<key>` in the old datas (see Event section).
  * `[new:<key>]`: The value of the `<key>` in the new datas (see Event section).
  * `[usable:<key>]`: The value of the `<key>` in the old (if DELETE) or new datas (see Event section).
  * `[data:<key>]`: The value of the `<key>` in the translated datas (see Event section).

Example:

```
{
  "translations": [
    {
      "output": {
        "driver": "Filesystem",
        "args": ["/tmp", "[name:basename].[name:extname]"]
      }
    }
  ]
}
```

#### Consul

This driver allow to store tranlated datas in [consul](https://www.consul.io/).

This driver takes two arguments:

* A consul connection object. This object can have the following keys:
  * `host` (String, default: 127.0.0.1): agent address.
  * `port` (Integer, default: 8500): agent HTTP(S) port.
  * `secure` (Boolean, default: false): enable HTTPS.
  * `ca` (String[], optional): array of strings or Buffers of trusted certificates in PEM format.
* A String containing the template of the base consul key. This template can use the following patterns :
  * `[name:full]`: The full name of the event (see Event section).
  * `[name:basename]`: The basename of the event name (see Event section).
  * `[name:extname]`: The extension of the event name (see Event section).
  * `[old:<key>]`: The value of the `<key>` in the old datas (see Event section).
  * `[new:<key>]`: The value of the `<key>` in the new datas (see Event section).
  * `[usable:<key>]`: The value of the `<key>` in the old (if DELETE) or new datas (see Event section).
  * `[data:<key>]`: The value of the `<key>` in the translated datas (see Event section).

Example:

```
{
  "translations": [
    {
      "output": {
        "driver": "Consul",
        "args": [
          {
            "secure": false,
            "host": "localhost",
            "port": 8500
          },
          "base/key/[name:full]/id_[usable:id]"
        ]
      }
    }
  ]
}
```

### Data processor Driver

#### JSONToJSON

This driver use [@biothings-explorer/json-transformer](https://github.com/kevinxin90/json_transformer.js) to transforme a JSON object using a JSON template.

This driver takes one argument : the json-transformer template in JSON format.

Example:

```
{
  "translations": [
    {
      "processor": {
        "driver": "JSONToJSON",
        "args": [
          {
            "ensembl": "ensembl.gene",
            "pathway": {
              "id": "wikipathway.id",
              "name": "wikipathway.name"
            }
          }
        ]
      }
    }
  ]
}
```

## Event

An event is a `PouchEvent` instance. This object contains the following attributes:

* `type` (ro) : the event type. It can only be `PouchEvent.ADD`, `PouchEvent.UPDATE` or `PouchEvent.DELETE`.
* `name` (ro) : the event name. The value of this field depend on the input driver. With the Filesystem input driver, this field contains the input filename ; with the Postgres input driver, this field is the bruild by concatenating the database schema name and the database table name, separeted by a dor (`.`).
* `newData` (ro) : the new data (from add or update). This field can only be undefined when the event type is `PouchEvent.DELETE`.
* `oldData` (ro) : the old data (from update or delete). This field can be undefined, depending on the input driver or when the event type is `PouchEvent.ADD`.
* `data` (rw) : the data translated by the data processor driver. This field can only be undefined when the event type is `PouchEvent.DELETE`.
* `usableData` (ro) : The _usable_ data. The value is the same as `newData` when `type` is `PouchEvent.ADD` or `PouchEvent.UPDATE`. When `type` is `PouchEvent.DELETE`, the value is the same as `oldData`.

## Create a Driver

### Input Driver

An input driver is class whose extands the `Input.Driver` class.

TBA

### Output Driver

An input driver is class whose extands the `Output.Driver` class.

TBA

### Data Driver

An input driver is a class whose extands the `Data.Driver` class.

This class must implement the `render/2` method. This method receive two parameters :

* A `PouchEvent` object (see Event)
* A callback who receive two parameters:
  * The updated `PouchEvent` object.
  * An error.

The `render/2` method should update the `data` field of the `PouchEvent` object.

Example:

This simple driver copy the _what_ data in the final data.

```
const Pouch = require('lib/drivers');

class CopyDriver extands Pouch.Data.Driver {
  constructor(what) {
    super();
    this.__what = what || 'usableData';
  }

  render(event, callback) {
    const data = event[this.__what];

    if (data) {
      event.data = data;
      callback(event, undefined);
      return;
    }

    callback(event, new Error(`no ${this.__what} available`);
  }
}
```

To use a custom driver, you must declare it via the `processor` key in the config. Under this key you use the `module` key (instead of the `driver` key) to specify the path to the driver file. You can use the `args` key to specify a list of parameters to pass to your driver constructor.

Example:

```
{
  "translations": [
    {
      "processor": {
        "module": "path/to/copy_driver.js",
        "args": ['old']
      }
    }
  ]
}
```

## Systemd configuration

Create the file `/etc/systemd/system/pouch.service` :
```
[Unit]
Description=Node.js Example Server
#Requires=After=mysql.service       # Requires the mysql service to run first

[Service]
ExecStart=<path>/node <path>/pouch/index.js -c <path>/pouch-config.json
Restart=always
RestartSec=10
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=nodejs-pouch
#User=<alternate user>
#Group=<alternate group>

[Install]
WantedBy=multi-user.target
```

Then run :

```
systemctl enable pouch.service
systemctl start pouch.service
systemctl status pouch.service
```

## License

Copyright (c) 2019, 2020 Grégoire Lejeune<br />
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
1. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
1. The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.


THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
