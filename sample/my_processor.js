const R = require('ramda');

const Drivers = require('../lib/drivers');

class MyProcessor extends Drivers.Data.Driver {
  constructor(arg0, arg1) {
    super();
    this._hello = arg0;
    this._world = arg1;
    this.logger.debug(`[MyProcessor] data driver initialized with ${arg0} and ${arg1}`);
  }

  render(event, callback) {
    const { usableData } = event;
    if (R.isNil(usableData)) {
      callback(event, undefined);
      return;
    }

    let parsedData = usableData;
    if (!R.is(Object, usableData) || R.is(Buffer, usableData)) {
      parsedData = JSON.parse(usableData);
    }

    event.data = JSON.stringify({ hello: this._hello, world: this._world, data: parsedData });
    callback(event, undefined);
  }
}
module.exports = MyProcessor;
