CREATE TABLE IF NOT EXISTS sample (
  id SERIAL PRIMARY KEY,
  name VARCHAR(128) NOT NULL CHECK (name <> ''),
  at DATE DEFAULT CURRENT_DATE
);

/*

show functions :
\df+

show triggers :
select * from pg_trigger;

*/
