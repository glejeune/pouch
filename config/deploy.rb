# frozen_string_literal: true

lock '~> 3.13'

set :service, 'pouch'

set :format_options, color: true
set :application, 'pouch'
set :repo_url, 'git@gitlab.com:glejeune/pouch.git'
set :keep_releases, 3
set :deploy_to, '/apps/pouch'

set :nvm_type, :user
set :nvm_node, 'v12.16.2'
set :nvm_map_bins, %w{node npm}

# server 'simple-web.site', user: 'sws', roles: [:app]

namespace :deploy do
  task :unmonitor do
    on roles(:app) do
      info "Unmonitor #{fetch :service}"
      execute "sudo monit unmonitor #{fetch :service}"
    end
  end

  task :restart_service do
    on roles(:app) do
      info "Restart #{fetch :service}"
      execute "sudo service #{fetch :service} restart"
    end
  end

  task :monitor do
    on roles(:app) do
      info "Monitor #{fetch :service}"
      execute "sudo monit monitor #{fetch :service}"
    end
  end

  before :starting, :unmonitor

  after :finishing, :restart_service
  after :restart_service, :monitor
end
