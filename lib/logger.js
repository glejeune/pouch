const log4js = require('log4js');

let log = console;

module.exports = {
  initialize: config => {
    const log4jsConfig = {
      appenders: {
        default: { type: 'file', filename: config.get('log.file') },
      },
      categories: {
        default: { appenders: ['default'], level: config.get('log.level') },
      },
    };

    if (config.get('log.stdout')) {
      log4jsConfig.appenders['out'] = { type: 'stdout' };
      log4jsConfig.categories.default.appenders.push('out');
    }

    log4js.configure(log4jsConfig);

    log = log4js.getLogger();
  },

  debug: (...args) => log.debug(...args),
  error: (...args) => log.error(...args),
  info: (...args) => log.info(...args),
  warning: (...args) => log.warn(...args),
  warn: (...args) => log.warn(...args),
};
