const EventEmitter = require('events');
const { fork } = require('child_process');
const R = require('ramda');
const log4js = require('log4js');
const { v4: uuidv4 } = require('uuid');

const log = log4js.getLogger();

class GetServerEmitter extends EventEmitter {}

const GenWorker = {
  __servers: {},
  __mailbox: {},
  __terminate: {},
  __onTerminate: {},
  __supervise: {},
  __emitter: new GetServerEmitter(),

  // Creates a GenWorker process.
  startLinks: (serverRef, module, ...params) => {
    if (GenWorker.__servers[serverRef]) {
      return { error: 'already_started', pid: GenWorker.__servers[serverRef] };
    }
    GenWorker.__mailbox[serverRef] = {};
    GenWorker.__servers[serverRef] = fork(`${__dirname}/gen_worker_process.js`);
    GenWorker.__servers[serverRef].send({ type: 'init', ref: serverRef, module, params });

    GenWorker.__servers[serverRef].on('message', msg => {
      switch (msg.type) {
        case 'call':
          GenWorker.__mailbox[serverRef][msg.client] = msg;
          GenWorker.__emitter.emit(msg.client);
          break;
        case 'terminate':
          GenWorker.__terminate[serverRef].reason = msg.reason;
          break;
        case 'log':
          log[msg.level](...msg.args);
          break;
        default:
          log.error(`${serverRef} invalid message type: ${msg.type}`);
      }
    });

    GenWorker.__servers[serverRef].on('exit', (_code, _signal) => {
      if (GenWorker.__terminate[serverRef] && GenWorker.__terminate[serverRef]['timeout']) {
        clearTimeout(GenWorker.__terminate[serverRef]);
      }

      const { reason } = GenWorker.__terminate[serverRef] || { reason: 'abnormal' };
      const onTerminateFun = GenWorker.__onTerminate[serverRef];
      const childSpec = GenWorker.__supervise[serverRef];

      log.debug(`Worker ${serverRef} terminated with reason ${reason}!`);

      delete GenWorker.__mailbox[serverRef];
      delete GenWorker.__servers[serverRef];
      delete GenWorker.__terminate[serverRef];
      delete GenWorker.__onTerminate[serverRef];
      delete GenWorker.__supervise[serverRef];

      if (reason === 'abnormal' && onTerminateFun && childSpec) {
        onTerminateFun(childSpec);
      }
    });

    log.debug(`Worker ${serverRef} started...`);
    return { ok: serverRef, pid: GenWorker.__servers[serverRef] };
  },

  // Orders a generic server to exit.
  // The GenWorker process calls Module:terminate/1 before exiting.
  stop: (serverRef, reason = 'normal', timeout = 'infinity') => {
    let signal = 'SIGKILL';
    if (reason === 'normal') {
      signal = 'SIGTERM';
    } else if (reason !== 'shutdown') {
      log.error(`Invalid reason (${reason}) use shutdown`);
    }
    if (timeout && timeout !== 'infinity') {
      GenWorker.__terminate[serverRef] = {
        timeout: setTimeout(() => {
          log.debug(`Kill ${serverRef} reason ${reason} (${signal})`);
          GenWorker.__servers[serverRef].kill(signal);
        }, timeout),
        reason: 'abnormal',
      };
    } else {
      GenWorker.__terminate[serverRef] = { reason: 'abnormal' };
    }

    GenWorker.__servers[serverRef].send({ type: 'terminate', reason });
  },

  // Makes a call to the serverRef of the GenWorker process by sending a
  // request and return a promise.
  // The GenWorker process calls Module:<fun/n> to handle the request.
  call: (serverRef, fun, ...params) => {
    const callID = uuidv4();
    GenWorker.__servers[serverRef].send({ type: 'call', client: callID, fun, params });
    return new Promise(resolve => {
      GenWorker.__emitter.once(callID, () => {
        const { response } = R.clone(GenWorker.__mailbox[serverRef][callID]);
        delete GenWorker.__mailbox[serverRef][callID];
        resolve(response);
      });
    });
  },

  // Makes a call to the serverRef of the GenWorker process by sending a
  // request and does not expect any response.
  // The GenWorker process calls Module:<fun/n> to handle the request.
  cast: (serverRef, fun, ...params) => {
    GenWorker.__servers[serverRef].send({ type: 'cast', fun, params });
  },

  // Enter in a main loop.
  // You can update the state at any time using GenWorker.state/1
  enterLoop: (serverRef, ...params) => {
    GenWorker.__servers[serverRef].send({ type: 'enter_loop', params });
  },

  // This function can be used by a gen_server process to explicitly send a reply
  // to a client that called call/2,n, when the reply cannot be defined in the return
  // value of Module:<fun/n>.
  reply: (client, response) => {
    process.send({ type: 'call', client, response });
  },

  state: state => {
    if (R.isNil(state)) {
      return R.clone(GenWorker.Process.__state);
    }
    GenWorker.Process.__state = R.clone(state);
    return state;
  },

  // This is a small object of log functions usables in the Worker
  log: {
    debug: (...args) => process.send({ type: 'log', level: 'debug', args }),
    info: (...args) => process.send({ type: 'log', level: 'info', args }),
    error: (...args) => process.send({ type: 'log', level: 'error', args }),
    warn: (...args) => process.send({ type: 'log', level: 'warn', args }),
    warning: (...args) => process.send({ type: 'log', level: 'warning', args }),
  },

  // Creates and supervise GenWorker
  // This function is to be called, directly.
  supervise: options => {
    if (R.isNil(options.childs) || R.isEmpty(options.childs)) {
      return { ok: 'nothing_to_start' };
    }

    const period = options.period || 5;
    const intensity = options.intensity || 1;
    return R.reduce(
      (acc, child) => {
        const { ok: childSpec, error: error0 } = GenWorker._checkChildSpec(child);
        if (error0) {
          let { error } = acc;
          error = error || [];
          return { ...acc, error: [...error, error0] };
        }
        const { ok: ok0, error: error1 } = GenWorker.startChild(period, intensity, childSpec);
        if (error1) {
          let { error } = acc;
          error = error || [];
          return { ...acc, error: [...error, error1] };
        }
        let { ok } = acc;
        ok = ok || [];
        return { ...acc, ok: [...ok, ok0] };
      },
      {},
      options.childs || []
    );
  },

  // Start a new supervised GenWorker child process
  startChild: (period, intensity, { id: serverRef, start: { module, function: fun, args }, restart }, options = {}) => {
    const __module = require(module);
    const result = __module[fun].apply(__module[fun], args);
    const { error } = result;
    if (R.isNil(error)) {
      GenWorker.__onTerminate[serverRef] = GenWorker._superviseRestart;
      GenWorker.__supervise[serverRef] = {
        period,
        intensity,
        childSpec: { id: serverRef, start: { module, function: fun, args }, restart },
        restart: options.restart || 0,
        lastStart: options.lastStart || Date.now(),
      };
    }
    return result;
  },

  _superviseRestart: ({ period, intensity, childSpec, restart, lastStart }) => {
    // No not restart a non permanent worker
    if (childSpec.restart !== 'permanent') {
      log.debug(`${childSpec.id} ${childSpec.restart} not restarted`);
      return { error: 'nothing_to_restart' };
    }

    const currentDate = Date.now();
    let lastStart0 = lastStart;
    let restart0 = restart;
    if (currentDate - lastStart > period * 1000) {
      lastStart0 = undefined;
      restart0 = 0;
    } else if (restart < intensity) {
      restart0 += 1;
    } else {
      log.error(`${childSpec.id} terminate abnormally`);
      return { error: 'terminate_abnormally' };
    }
    const response = GenWorker.startChild(period, intensity, childSpec, {
      restart: restart0,
      lastStart: lastStart0,
    });
    if (response.error) {
      log.error(`Failed to restart ${childSpec.id}, reason: ${response.error}`);
    }
    return response;
  },

  _checkChildSpec: ({ id, start, restart }) => {
    if (R.isNil(id)) {
      return { error: 'missing_id' };
    }
    if (R.isNil(start)) {
      return { error: 'missing_mfa' };
    }
    const { module, function: fun } = start;
    if (R.isNil(module)) {
      return { error: 'missing_mfa_module' };
    }
    if (R.isNil(fun)) {
      return { error: 'missing_mfa_function' };
    }
    let { args } = start;
    if (R.isNil(args)) {
      args = [];
    }
    let restart0 = restart;
    if (R.isNil(restart0)) {
      restart0 = 'permanent';
    }
    if (restart0 !== 'permanent' && restart0 !== 'temporary') {
      return { error: 'invalid_restart' };
    }
    return {
      ok: { id, start: { module, function: fun, args }, restart: restart0 },
    };
  },

  Process: {
    __state: undefined,
    __module: undefined,

    init: (serverRef, moduleRef, params) => {
      process.title = serverRef;
      if (R.is(Array, moduleRef)) {
        GenWorker.Process.__module = R.reduce(
          (acc, ref) => {
            const __ref = require(ref);
            return { ...acc, ...__ref };
          },
          {},
          moduleRef
        );
      } else {
        GenWorker.Process.__module = require(moduleRef);
      }
      if (GenWorker.Process.__module.init) {
        GenWorker.Process.__state = GenWorker.Process.__module.init.apply(GenWorker.Process.__module.init, params);
      }
    },

    call: (client, fun, ...params) => {
      if (R.isNil(GenWorker.Process.__module[fun])) {
        log.error(`${fun} is not defined`);
        GenWorker.reply(client, { error: `${fun} is not defined` });
        return;
      }

      const { status, response, state } = GenWorker.Process.__module[fun].apply(GenWorker.Process.__module[fun], [
        client,
        ...params,
        R.clone(GenWorker.Process.__state),
      ]);

      GenWorker.Process._handleResponse(client, status, response, state);
    },

    cast: (fun, ...params) => {
      if (R.isNil(GenWorker.Process.__module[fun])) {
        log.error(`${fun} is not defined`);
        return;
      }

      const { status, state } = GenWorker.Process.__module[fun].apply(GenWorker.Process.__module[fun], [
        ...params,
        R.clone(GenWorker.Process.__state),
      ]);

      if (state) {
        GenWorker.Process.__state = R.clone(state);
      } else {
        log.error(`Missing state`);
        return;
      }

      switch (status) {
        case 'noreply':
          break;
        case undefined:
          log.error('Missing response type');
          break;
        default:
          log.error(`Invalid response type (${status})`);
      }
    },

    enterLoop: (client, ...params) => {
      if (GenWorker.Process.__module.mainLoop) {
        const { status, response, state } = GenWorker.Process.__module.mainLoop.apply(
          GenWorker.Process.__module.mainLoop,
          [client, ...params, R.clone(GenWorker.Process.__state)]
        );

        GenWorker.Process._handleResponse(client, status, response, state);
      }
    },

    _handleResponse: (client, status, response, state) => {
      if (state) {
        GenWorker.Process.__state = R.clone(state);
      } else {
        log.error('Missing state');
        GenWorker.reply(client, { error: 'Missing state' });
        return;
      }

      switch (status) {
        case 'reply':
          GenWorker.reply(client, { ok: response });
          break;
        case 'noreply':
          break;
        case undefined:
          log.error('Missing response type');
          GenWorker.reply(client, { error: 'Missing response type' });
          break;
        default:
          log.error(`Invalid response type (${status})`);
          GenWorker.reply(client, { error: `Invalid response type (${status})` });
      }
    },

    terminate: reason => {
      if (GenWorker.Process.__module.terminate) {
        GenWorker.Process.__module.terminate(reason, GenWorker.Process.__state);
      }
      process.send({ type: 'terminate', reason });
      process.disconnect();
    },
  },
};

module.exports = GenWorker;
