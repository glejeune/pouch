const path = require('path');
const R = require('ramda');

const PouchEvent = require('../event');
const { ImplementationDriverError, EventTypeError } = require('../errors');
const GenWorker = require('../../gen_worker');

class PouchOutputDriver {
  constructor() {
    this.logger = GenWorker.log;
  }

  callWriter(event) {
    return new Promise((resolve, reject) => {
      if (!R.is(PouchEvent, event)) {
        reject(new EventTypeError());
      } else {
        try {
          this.writer(event, error => {
            if (error) {
              reject(error);
            }
            resolve();
          });
        } catch (error) {
          reject(error);
        }
      }
    });
  }

  writer(_event) {
    throw new ImplementationDriverError(`writer method not implemented in ${this.constructor.name}`);
  }

  eventToString(model, event) {
    const extname = path.extname(event.name);
    const basename = path.basename(event.name, extname);
    return model
      .replace(/\[name:full\]/g, event.name)
      .replace(/\[name:basename\]/g, basename)
      .replace(/\[name:extname\]/g, extname.replace(/^\./, ''))
      .replace(/\[data:([^\]]*)\]/g, (_match, p1, _offset, _string) => event.data[p1])
      .replace(/\[old:([^\]]*)\]/g, (_match, p1, _offset, _string) => event.oldData[p1])
      .replace(/\[usable:([^\]]*)\]/g, (_match, p1, _offset, _string) => event.usableData[p1])
      .replace(/\[new:([^\]]*)\]/g, (_match, p1, _offset, _string) => event.newData[p1]);
  }
}

module.exports = PouchOutputDriver;
