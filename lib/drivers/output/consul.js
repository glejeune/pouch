const R = require('ramda');
const consul = require('consul');

const PouchOutputDriver = require('./driver');
const PouchEvent = require('../event');

class Consul extends PouchOutputDriver {
  constructor(conn, base) {
    super();
    this.__baseKey = base || '';
    this.__client = consul(conn);
    this.logger.debug(`[Driver.Output.Consul] driver initialized with base : ${this.__baseKey}`);
  }

  writer(event, callback) {
    const baseKey = this.eventToString(this.__baseKey, event);
    if (event.type === PouchEvent.DELETE) {
      const data = JSON.parse(event.data);
      this._del(R.keys(data), baseKey, callback);
    } else {
      const data = JSON.parse(event.data);
      this._set(R.keys(data), data, baseKey, callback);
    }
  }

  _set(keys, data, baseKey, callback) {
    if (R.isEmpty(keys)) {
      callback(undefined);
      return;
    }

    const [key, ...rest] = keys;
    const finalKey = `${baseKey}/${key}`;

    this.logger.debug(`[Driver.Output.Consul] set value for key ${finalKey}:`, data[key]);
    this.__client.kv.set(finalKey, JSON.stringify(data[key]), (error, _result) => {
      if (error) {
        callback(error);
        return;
      }
      this._set(rest, data, baseKey, callback);
    });
  }

  _del(keys, baseKey, callback) {
    if (R.isEmpty(keys)) {
      callback(undefined);
      return;
    }

    const [key, ...rest] = keys;
    const finalKey = `${baseKey}/${key}`;

    this.logger.debug(`[Driver.Output.Consul] det key ${finalKey}`);
    this.__client.kv.del(finalKey, error => {
      if (error) {
        callback(error);
        return;
      }
      this._del(rest, baseKey, callback);
    });
  }
}

module.exports = Consul;
