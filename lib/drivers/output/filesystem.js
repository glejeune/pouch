const fs = require('fs');
const path = require('path');

const PouchOutputDriver = require('./driver');
const PouchEvent = require('../event');

class Filesystem extends PouchOutputDriver {
  constructor(root, outputFormat) {
    super();
    this.__root = root;
    this.__outputFormat = outputFormat || '[name:full]';
    this.logger.debug(
      `[Driver.Output.Filesystem] driver initialized with root: ${root}, outputFormat: ${outputFormat}`
    );
  }

  writer(event, callback) {
    const output = path.join(this.__root, this.eventToString(this.__outputFormat, event));
    this.logger.debug(`[Driver.Output.Filesystem] ${event.type} ${output}...`);
    if (event.type === PouchEvent.DELETE) {
      fs.unlink(output, callback);
    } else {
      fs.writeFile(output, event.data, callback);
    }
  }
}

module.exports = Filesystem;
