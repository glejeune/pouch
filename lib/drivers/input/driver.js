const R = require('ramda');

const PouchEvent = require('../event');
const { ImplementationDriverError, ProcessorDriverError, OutputDriverError, EventTypeError } = require('../errors');
const GenWorker = require('../../gen_worker');

class PouchInputDriver {
  constructor() {
    this.logger = GenWorker.log;
    this.__processor = undefined;
    this.__output = undefined;
  }

  set processor(processor) {
    this.__processor = processor;
  }

  set output(output) {
    this.__output = output;
  }

  handle(event) {
    if (!R.is(PouchEvent, event)) {
      throw new EventTypeError();
    }

    if (R.isNil(this.__processor)) {
      throw new ProcessorDriverError();
    }

    if (R.isNil(this.__output)) {
      throw new OutputDriverError();
    }

    this.__processor
      .callRender(event)
      .then(finalEvent => this.__output.callWriter(finalEvent))
      .catch(error => {
        this.logger.error(error);
        throw error;
      });
  }

  start(_processor, _output) {
    throw new ImplementationDriverError(`start method not implemented in ${this.constructor.name}`);
  }
}

module.exports = PouchInputDriver;
