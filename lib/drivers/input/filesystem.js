const fs = require('fs');
const path = require('path');
const R = require('ramda');
const chokidar = require('chokidar');

const PouchInputDriver = require('./driver');
const PouchEvent = require('../event');

class Filesystem extends PouchInputDriver {
  constructor(root, chokidarOptions) {
    super();
    this.__root = root;
    if (chokidarOptions && chokidarOptions.ignored) {
      chokidarOptions.ignored = new RegExp(chokidarOptions.ignored);
    }
    this.__chokidarOptions = chokidarOptions || {};
    this.logger.debug(`[Driver.Input.Filesystem] driver initialized with root: ${root} and chokidar:`, chokidarOptions);
  }

  start() {
    this.__watcher = chokidar
      .watch(this.__root, this.__chokidarOptions)
      .on('add', R.curry(this._dispatch)(this, PouchEvent.ADD))
      .on('change', R.curry(this._dispatch)(this, PouchEvent.UPDATE))
      .on('unlink', R.curry(this._dispatch)(this, PouchEvent.DELETE))
      .on('error', error => {
        if (error.code !== 'ENOENT') {
          this.logger.error(`[Driver.Input.Filesystem] An error occured while watching files ${this.__patn}:`, error);
        }
      });
  }

  _dispatch(self, event, filename) {
    self.logger.debug(`[Driver.Input.Filesystem] ${event} ${filename}...`);

    if (event !== PouchEvent.DELETE) {
      fs.readFile(filename, (error, data) => {
        if (error) {
          self.logger.error(`[Driver.Input.Filesystem] failed to read ${filename}:`, error);
          return;
        }

        self.handle(PouchEvent.build({ type: event, name: path.basename(filename), newData: data }));
      });
      return;
    }
    self.handle(PouchEvent.build({ type: event, name: path.basename(filename) }));
  }
}

module.exports = Filesystem;
