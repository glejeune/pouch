const R = require('ramda');
const pg = require('pg');

const PouchInputDriver = require('./driver');
const PouchEvent = require('../event');

const DEFAULT_PG_RETRY = 5;
const DEFAULT_PG_RETRY_WAIT = 1000;
const EVENT = {
  INSERT: PouchEvent.ADD,
  UPDATE: PouchEvent.UPDATE,
  DELETE: PouchEvent.DELETE,
};

class Postgres extends PouchInputDriver {
  constructor(tables, conn) {
    super();
    this.__tables = tables;
    this.__retry = conn.retry || DEFAULT_PG_RETRY;
    this.__retryWait = conn.retryWait || DEFAULT_PG_RETRY_WAIT;
    this.__pgConnectionString = `postgres://${conn.user}:${conn.password}@${conn.host || 'localhost'}:${conn.port ||
      5432}/${conn.database}`;
    this.__client = new pg.Client(this.__pgConnectionString);
    this._connect()
      .then(_res => this._createFunctions())
      .then(_res => this._createTriggers())
      .catch(error => this.logger.error(error));
    this.logger.debug(`[Driver.Input.Postgres] driver initialized for tables:`, tables);
  }

  start() {
    const _query = this.__client.query('LISTEN pouch_events');
    this.__client.on('notification', async data => {
      const payload = JSON.parse(data.payload);
      this.handle(
        PouchEvent.build({
          type: EVENT[payload.operation],
          name: `${payload.schema}.${payload.table}`,
          newData: this._data(payload.new),
          oldData: this._data(payload.old),
        })
      );
    });
  }

  _data(data) {
    if (R.isNil(data) || R.isEmpty(data)) {
      return undefined;
    }
    return data;
  }

  _connect() {
    return new Promise((resolve, reject) => {
      this._pgConnect(resolve, reject, 1);
    });
  }

  _pgConnect(resolve, reject, count) {
    this.__client.connect(err => {
      if (err) {
        if (count < this.__retry) {
          this.logger.error(`PG connection failed ${count}/${this.__retry}`);
          setTimeout(() => this._pgConnect(resolve, reject, count + 1), this.__retryWait);
        } else {
          reject(new Error("Can't connect to PG :("));
        }
      } else {
        resolve(this.__client);
      }
    });
  }

  _createFunctions() {
    const fun = `CREATE OR REPLACE FUNCTION public.notify_pouch_event()
  RETURNS trigger
  LANGUAGE plpgsql
AS $function$
BEGIN
  IF (TG_OP = 'INSERT') THEN
    PERFORM pg_notify('pouch_events', '{"schema": "' || TG_TABLE_SCHEMA || '", "operation": "' || TG_OP || '", "table": "' || TG_TABLE_NAME || '", "new": ' || row_to_json(NEW)::text || '}');
  ELSIF (TG_OP = 'DELETE') THEN
    PERFORM pg_notify('pouch_events', '{"schema": "' || TG_TABLE_SCHEMA || '", "operation": "' || TG_OP || '", "table": "' || TG_TABLE_NAME || '", "old": ' || row_to_json(OLD)::text || '}');
  ELSE
    PERFORM pg_notify('pouch_events', '{"schema": "' || TG_TABLE_SCHEMA || '", "operation": "' || TG_OP || '", "table": "' || TG_TABLE_NAME || '", "new": ' || row_to_json(NEW)::text || ', "old": ' || row_to_json(OLD)::text || '}');
  END IF;
  RETURN NULL;
END;
$function$`;
    return this.__client.query(fun);
  }

  _createTriggers() {
    return Promise.all(
      R.forEach(
        table =>
          this.__client
            .query(`DROP TRIGGER IF EXISTS ${table}_pouch_trigger ON ${table}`)
            .then(_res =>
              this.__client.query(
                `CREATE TRIGGER ${table}_pouch_trigger AFTER INSERT OR UPDATE OR DELETE ON ${table} FOR EACH ROW EXECUTE PROCEDURE public.notify_pouch_event()`
              )
            ),
        this.__tables
      )
    );
  }
}

module.exports = Postgres;
