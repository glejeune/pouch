module.exports = {
  Data: require('./data'),
  Input: require('./input'),
  Output: require('./output'),
};
