class ImplementationDriverError extends Error {
  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, ImplementationDriverError);
  }
}

class InputDriverError extends Error {
  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, ImplementationDriverError);
  }
}

class ProcessorDriverError extends Error {
  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, ImplementationDriverError);
  }
}

class OutputDriverError extends Error {
  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, ImplementationDriverError);
  }
}

class EventTypeError extends Error {
  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, ImplementationDriverError);
  }
}

module.exports = {
  ImplementationDriverError,
  InputDriverError,
  OutputDriverError,
  ProcessorDriverError,
  EventTypeError,
};
