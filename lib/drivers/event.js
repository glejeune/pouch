const R = require('ramda');

const DELETE = 'delete';
const ADD = 'add';
const UPDATE = 'update';

class PouchEvent {
  static build({ type, name, oldData, newData }) {
    return new PouchEvent(type, name, oldData, newData);
  }

  static get DELETE() {
    return DELETE;
  }

  static get ADD() {
    return ADD;
  }

  static get UPDATE() {
    return UPDATE;
  }

  constructor(type, name, oldData, newData) {
    this.__type = type;
    this.__name = name;
    this.__newData = newData;
    this.__oldData = oldData;
    this.__data = undefined;
  }

  get newData() {
    return R.clone(this.__newData);
  }

  get oldData() {
    return R.clone(this.__oldData);
  }

  get usableData() {
    let usableData = R.clone(this.__newData);
    if (this.__type === DELETE) {
      usableData = R.clone(this.__oldData);
    }
    return usableData;
  }

  get name() {
    return R.clone(this.__name);
  }

  get type() {
    return R.clone(this.__type);
  }

  get data() {
    return R.clone(this.__data);
  }

  set data(data) {
    this.__data = data;
  }
}

module.exports = PouchEvent;
