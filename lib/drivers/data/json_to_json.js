const R = require('ramda');

const PouchDataDriver = require('./driver');

class JSONToJSON extends PouchDataDriver {
  constructor(template) {
    super();
    this.__engine = require('@biothings-explorer/json-transformer');
    this.__template = template;
    this.logger.debug('[Driver.Data.JSONToJSON] driver initialized');
  }

  render(event, callback) {
    const { usableData } = event;
    if (R.isNil(usableData)) {
      callback(event, undefined);
      return;
    }

    let parsedData = usableData;
    if (!R.is(Object, usableData) || R.is(Buffer, usableData)) {
      parsedData = JSON.parse(usableData);
    }

    this.logger.debug(
      `[Driver.Data.JSONToJSON] render ${JSON.stringify(parsedData)} with ${JSON.stringify(this.__template)}`
    );

    try {
      event.data = this.__engine(parsedData, this.__template);
      callback(event, undefined);
    } catch (error) {
      callback(event, error);
    }
  }
}

module.exports = JSONToJSON;
