const R = require('ramda');

const PouchEvent = require('../event');
const { ImplementationDriverError, EventTypeError } = require('../errors');
const GenWorker = require('../../gen_worker');

class PouchDataDriver {
  constructor() {
    this.logger = GenWorker.log;
  }

  callRender(event) {
    return new Promise((resolve, reject) => {
      if (!R.is(PouchEvent, event)) {
        reject(new EventTypeError());
      } else {
        try {
          this.render(event, (value, error) => {
            if (error) {
              reject(error);
            }
            resolve(value);
          });
        } catch (error) {
          reject(error);
        }
      }
    });
  }

  render(_event) {
    throw new ImplementationDriverError(`render method not implemented in ${this.constructor.name}`);
  }
}

module.exports = PouchDataDriver;
