const GenWorker = require('./gen_worker');

process.on('message', msg => {
  switch (msg.type) {
    case 'init':
      GenWorker.Process.init(msg.ref, msg.module, msg.params);
      break;
    case 'call':
      GenWorker.Process.call(msg.client, msg.fun, ...msg.params);
      break;
    case 'cast':
      GenWorker.Process.cast(msg.fun, ...msg.params);
      break;
    case 'enter_loop':
      GenWorker.Process.enterLoop(msg.client, ...msg.params);
      break;
    case 'terminate':
      GenWorker.Process.terminate(msg.reason);
      break;
    default:
  }
});
