const GenWorker = require('./gen_worker');
const Drivers = require('./drivers');

const Translator = {
  run: async ({ name, intensity, period, restart, processor, input, output }) => {
    GenWorker.supervise({
      intensity: intensity || 1,
      period: period || 5,
      childs: [
        {
          id: name,
          start: {
            module: __filename,
            function: 'startLinks',
            args: [name, processor, input, output],
          },
          restart: restart || 'permanent',
        },
      ],
    });
    GenWorker.enterLoop(name);
  },

  startLinks: (name, processor, input, output) => GenWorker.startLinks(name, __filename, processor, input, output),

  init: (processor, input, output) => ({ processor, input, output }),

  mainLoop: (client, state) => {
    const { processor, input, output } = state;
    const inputObj = Translator._loadDriver(Drivers.Input, input);
    const processorObj = Translator._loadDriver(Drivers.Data, processor);
    const outputObj = Translator._loadDriver(Drivers.Output, output);
    inputObj.processor = processorObj;
    inputObj.output = outputObj;
    inputObj.start();

    GenWorker.log.debug('Main loop started with', state);
    return { status: 'noreply', state };
  },

  // --

  _loadDriver: (type, processor) => {
    let Processor;
    const args = processor.args || [];
    if (processor.module) {
      Processor = require(processor.module);
    } else {
      Processor = type[processor.driver];
    }
    const result = new Processor(...args);
    return result;
  },
};

module.exports = Translator;
