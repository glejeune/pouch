const should = require('should');
const sinon = require('sinon');

const GenWorker = require('../../../lib/gen_worker');
const JSONToJSON = require('../../../lib/drivers/data/json_to_json');
const PouchEvent = require('../../../lib/drivers/event');

describe('Driver.Data.JSONToJSON', () => {
  let sandbox;

  beforeEach(() => {
    sandbox = sinon.createSandbox();

    sandbox.stub(GenWorker.log, 'debug').returns();
    sandbox.stub(GenWorker.log, 'info').returns();
    sandbox.stub(GenWorker.log, 'error').returns();
    sandbox.stub(GenWorker.log, 'warn').returns();
    sandbox.stub(GenWorker.log, 'warning').returns();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('when there is no usable data', () => {
    it('should not call the engine', done => {
      const event = PouchEvent.build({
        type: PouchEvent.DELETE,
        name: 'pouch-event.sample',
      });

      const driver = new JSONToJSON({});
      const stubEngine = sandbox.stub(driver, '__engine');

      driver.render(event, (newEvent, error) => {
        should.not.exist(error);
        should.not.exist(newEvent.data);
        sinon.assert.notCalled(stubEngine);
        done();
      });
    });
  });

  describe('when there is usable data', () => {
    it('should call the engine and set event data', done => {
      const event = PouchEvent.build({
        type: PouchEvent.ADD,
        name: 'pouch-event.sample',
        newData: { newData: 'new-data' },
      });
      const driver = new JSONToJSON({});
      const stubEngine = sandbox.stub(driver, '__engine').returns('generated-data');

      driver.render(event, (newEvent, error) => {
        should.not.exist(error);
        newEvent.data.should.equal('generated-data');
        sinon.assert.calledOnce(stubEngine);
        sinon.assert.calledWith(stubEngine, { newData: 'new-data' }, {});
        done();
      });
    });
  });

  describe('when the engine throw and exception', () => {
    it('should pass the exception to the callback', done => {
      const event = PouchEvent.build({
        type: PouchEvent.ADD,
        name: 'pouch-event.sample',
        newData: { newData: 'new-data' },
      });
      const driver = new JSONToJSON({});
      const stubEngine = sandbox.stub(driver, '__engine').throws();

      driver.render(event, (newEvent, error) => {
        should.exist(error);
        should.not.exist(newEvent.data);
        sinon.assert.calledOnce(stubEngine);
        sinon.assert.calledWith(stubEngine, { newData: 'new-data' }, {});
        done();
      });
    });
  });
});
