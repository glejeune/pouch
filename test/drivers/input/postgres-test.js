const should = require('should');
const sinon = require('sinon');
const pg = require('pg');

const GenWorker = require('../../../lib/gen_worker');
const Postgres = require('../../../lib/drivers/input/postgres');
const PouchEvent = require('../../../lib/drivers/event');

describe('Driver.Input.Postgres', () => {
  let sandbox;
  let driver;

  beforeEach(() => {
    sandbox = sinon.createSandbox();

    sandbox.stub(GenWorker.log, 'debug').returns();
    sandbox.stub(GenWorker.log, 'info').returns();
    sandbox.stub(GenWorker.log, 'error').returns();
    sandbox.stub(GenWorker.log, 'warn').returns();
    sandbox.stub(GenWorker.log, 'warning').returns();

    sandbox.stub(pg.Client.prototype, 'connect').callsFake(callback => callback());
    sandbox.stub(pg.Client.prototype, 'query').returns('fake');

    driver = new Postgres(['table'], { user: 'test', password: 'test', database: 'test' });
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should handle an ADD event when a row is inserted', done => {
    sandbox.stub(driver, 'handle').callsFake(event => {
      event.type.should.equal(PouchEvent.ADD);
      event.name.should.equal('test.table');
      event.newData.should.eql('data-content');
      should.not.exist(event.oldData);
      should.not.exist(event.data);
      done();
    });

    driver.start();

    driver.__client.emit('notification', {
      payload: '{"schema": "test", "operation": "INSERT", "table": "table", "new": "data-content", "old": ""}',
    });
  });

  it('should handle an UPDATE event when a row is updated', done => {
    sandbox.stub(driver, 'handle').callsFake(event => {
      event.type.should.equal(PouchEvent.UPDATE);
      event.name.should.equal('test.table');
      event.newData.should.eql('new-data-content');
      event.oldData.should.eql('old-data-content');
      should.not.exist(event.data);
      done();
    });

    driver.start();

    driver.__client.emit('notification', {
      payload:
        '{"schema": "test", "operation": "UPDATE", "table": "table", "new": "new-data-content", "old": "old-data-content"}',
    });
  });

  it('should handle a DELETE event when a row is deleted', done => {
    sandbox.stub(driver, 'handle').callsFake(event => {
      event.type.should.equal(PouchEvent.DELETE);
      event.name.should.equal('test.table');
      should.not.exist(event.newData);
      event.oldData.should.eql('data-content');
      should.not.exist(event.data);
      done();
    });

    driver.start();

    driver.__client.emit('notification', {
      payload: '{"schema": "test", "operation": "DELETE", "table": "table", "new": "", "old": "data-content"}',
    });
  });
});
