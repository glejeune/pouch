const should = require('should');
const sinon = require('sinon');
const FS = require('fs-mock');
const rewire = require('rewire');

const GenWorker = require('../../../lib/gen_worker');
const PouchEvent = require('../../../lib/drivers/event');

describe('Driver.Input.Filesystem', () => {
  let sandbox;
  let Filesystem;
  let driver;

  beforeEach(() => {
    sandbox = sinon.createSandbox();

    sandbox.stub(GenWorker.log, 'debug').returns();
    sandbox.stub(GenWorker.log, 'info').returns();
    sandbox.stub(GenWorker.log, 'error').returns();
    sandbox.stub(GenWorker.log, 'warn').returns();
    sandbox.stub(GenWorker.log, 'warning').returns();

    Filesystem = rewire('../../../lib/drivers/input/filesystem');
    driver = new Filesystem('/tmp/tests/*.json');
  });

  afterEach(() => {
    sandbox.restore();
    driver.__watcher.close();
  });

  it('should handle an ADD event when a file is added', done => {
    Filesystem.__set__(
      'fs',
      new FS({
        '/tmp/tests/test.json': 'json-content',
      })
    );

    sandbox.stub(driver, 'handle').callsFake(event => {
      event.type.should.equal(PouchEvent.ADD);
      event.name.should.equal('test.json');
      event.newData.should.eql(Buffer.from('json-content'));
      should.not.exist(event.oldData);
      should.not.exist(event.data);
      done();
    });

    driver.start();

    driver.__watcher.emit('add', '/tmp/tests/test.json');
  });

  it('should handle an UPDATE event when a file is updated', done => {
    Filesystem.__set__(
      'fs',
      new FS({
        '/tmp/tests/test.json': 'json-content',
      })
    );

    sandbox.stub(driver, 'handle').callsFake(event => {
      event.type.should.equal(PouchEvent.UPDATE);
      event.name.should.equal('test.json');
      event.newData.should.eql(Buffer.from('json-content'));
      should.not.exist(event.oldData);
      should.not.exist(event.data);
      done();
    });

    driver.start();

    driver.__watcher.emit('change', '/tmp/tests/test.json');
  });

  it('should handle a DELETE event when a file is updated', done => {
    sandbox.stub(driver, 'handle').callsFake(event => {
      event.type.should.equal(PouchEvent.DELETE);
      event.name.should.equal('test.json');
      should.not.exist(event.newData);
      should.not.exist(event.oldData);
      should.not.exist(event.data);
      done();
    });

    driver.start();

    driver.__watcher.emit('unlink', '/tmp/tests/test.json');
  });
});
