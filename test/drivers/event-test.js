const should = require('should');

const PouchEvent = require('../../lib/drivers/event');

describe('PouchEvent', () => {
  it('should build a complete event', () => {
    const event = PouchEvent.build({
      type: PouchEvent.UPDATE,
      name: 'pouch-event.sample',
      oldData: 'old-data',
      newData: 'new-data',
    });

    event.type.should.equal(PouchEvent.UPDATE);
    event.name.should.equal('pouch-event.sample');
    event.oldData.should.equal('old-data');
    event.newData.should.equal('new-data');
  });

  it('should send new data as usable data when type === ADD', () => {
    const event = PouchEvent.build({
      type: PouchEvent.ADD,
      name: 'pouch-event.sample',
      newData: 'new-data',
    });

    event.type.should.equal(PouchEvent.ADD);
    event.name.should.equal('pouch-event.sample');
    should.not.exist(event.oldData);
    event.usableData.should.equal('new-data');
  });

  it('should send new data as usable data when type === UPDATE', () => {
    const event = PouchEvent.build({
      type: PouchEvent.UPDATE,
      name: 'pouch-event.sample',
      newData: 'new-data',
      oldData: 'old-data',
    });

    event.type.should.equal(PouchEvent.UPDATE);
    event.name.should.equal('pouch-event.sample');
    event.oldData.should.equal('old-data');
    event.newData.should.equal('new-data');
    event.usableData.should.equal('new-data');
  });

  it('should send old data as usable data when type === DELETE', () => {
    const event = PouchEvent.build({
      type: PouchEvent.DELETE,
      name: 'pouch-event.sample',
      oldData: 'old-data',
    });

    event.type.should.equal(PouchEvent.DELETE);
    event.name.should.equal('pouch-event.sample');
    event.oldData.should.equal('old-data');
    should.not.exist(event.newData);
    event.usableData.should.equal('old-data');
  });
});
